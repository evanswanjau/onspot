$(document).ready(function(){

  // scroll to div functionality
  $(".scroll").click(function() {
    $('html,body').animate({
        scrollTop: $(".what_we_can_do").offset().top},
          1000, 'easeInOutCirc');
  });

  $(".about").click(function() {
    $('html,body').animate({
        scrollTop: $(".about_border").offset().top},
          1000, 'easeInOutCirc');
  });


  // top menu reveal and hide on scroll
  $('.regular').hide();
  $(document).scroll(function(){

      if($(this).scrollTop() > 200){

        $('.top-menu').css({'background-color':'white'});
        $('.top-menu a').css({'color':'#333'});
        $('.hamburger-menu i').css({'color':'#0F0672'});
        $('.top-menu').css({'box-shadow':'0 0 5px 2px rgba(0, 0, 0, 0.2)'});
        $('.white').hide();
        $('.regular').show();
        $('.coco').css({'background-color':'#fff', 'box-shadow':'0 0 5px 2px rgba(0, 0, 0, 0.2)!important'});
        $('.coco a').css({'color':'#444'});
      }

      if($(this).scrollTop() < 200){

        $('.top-menu').css({'background-color':'transparent'});
        $('.top-menu a').css({'color':'white'});
        $('.hamburger-menu i').css({'color':'#fff'});
        $('.top-menu').css({'box-shadow':'none'});
        $('.white').show();
        $('.regular').hide();
        $('.coco').css({'background-color':'#fff!important', 'box-shadow':'0 0 5px 2px rgba(0, 0, 0, 0.2)!important!important'});
        $('.coco a').css({'color':'#444!important'});
      }
    });


  // image scroll animation
  var $win = $(window);

    $('.image_container, .welcome_to_about').each(function(){
        var scroll_speed = 2;
        var $this = $(this);
        $(window).scroll(function() {
            var bgScroll = -(($win.scrollTop() - $this.offset().top)/ scroll_speed);
            var bgPosition = 'center '+ bgScroll + 'px';
            $this.css({ backgroundPosition: bgPosition });
        });
    });



    // customer quotes infinite scrolling
    $('.2').hide();
    $('.3').hide();
    $('.1').delay(7000).hide("slide", {'direction':'left'}, 2000, 'easeInOutCirc');
    $('.2').delay(7000).show("slide", {'direction':'right'}, 2000, 'easeInOutCirc');
    $('.2').delay(7000).hide("slide", {'direction':'left'}, 2000, 'easeInOutCirc');
    $('.3').delay(17000).show("slide", {'direction':'right'}, 2000, 'easeInOutCirc');



    // menu styling
    $('.hidden-menu').hide();
    $('.hamburger-menu').click(function(){
      $('.hidden-menu').show("slide", {'direction':'left'}, 1000, 'easeInOutCirc');
      $('.hamburger-menu').hide();
    });

    $('.closeit').click(function(){
      $('.hidden-menu').hide("slide", {'direction':'left'}, 1000, 'easeInOutCirc');
      $('.hamburger-menu').show();
    });

});
