/*
* MAIN JAVASCRIPT FUNCTIONS
*/

// FUNCTION TO SEND MESSAGE TO ADMIN
function sendMessage(){

  var inputName = document.forms["myForm"]["name"].value;
  var inputPhone = document.forms["myForm"]["phone"].value;
  var inputPackage = document.forms["myForm"]["package"].value;
  var inputMessage = document.forms["myForm"]["message"].value;

  if (inputName != '' && inputPhone != '' && inputMessage != '') {

    var json_message_string = '{"name":"'+inputName+'", "phone":"'+inputPhone+'", "package":"'+inputPackage+'", "message":"'+inputMessage+'"}';

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {

      if (this.readyState == 4 && this.status == 200) {
        if (this.responseText == 1) {
          document.getElementById('message').innerHTML = "<h4>Thanks for your feedback.</h4><h4>We'll get back to you shortly.</h4><br>";
          document.getElementById('message').style.color = "#1DA361";
          document.getElementById('sendMessage').innerHTML = '<button class="sent" type="button"><i class="fa fa-check-circle"></i></button>';
        }else {
          document.getElementById('message').innerHTML = this.responseText;
          document.getElementById('message').style.color = "#ff5353";
        }
      }

    };

    xmlhttp.open("POST", "../engine/cogs.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("feedback=" + json_message_string);
  }

  return false;
}

function inputValue(value, id){

  if (value.length < 1) {
    $(document).ready(function(){
      document.getElementById(''+id+'').style.border = '2px solid #ff5353';
      $('#'+id+'').effect('shake', {direction:'down', times:2, distance:5}, 600);
    });
  } else {
    document.getElementById(''+id+'').style.border = '2px solid #1DA361';
    document.getElementById(''+id+'').style.color = '#888';
  }

  return false;
}
