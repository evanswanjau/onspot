<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Training</title>
    <meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
    <meta name="description" content="Your Technology Solutions Partner">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css">
    <script type="text/javascript" src="../js/script.js"></script>
    <link rel="shortcut icon" href="../favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>


    <!-- Mobile Menu Section -->
    <section>
      <!-- Hamburger Menu -->
      <div class="hamburger-menu">
        <i class="fa fa-bars"></i>
      </div>

      <div class="hidden-menu">
        <div class="closeit right">
          <i class="fa fa-times" title="close"></i>
        </div>
        <ul>
          <h3>MENU</h3>
          <br><br>
          <a href="../"><li>HOME</li></a>
          <a href="../about-us"><li>ABOUT US</li></a>
          <a href="../services"><li>SERVICES</li></a>
          <a href="../training"><li>TRAINING</li></a>
          <a href="../careers"><li>CAREERS</li></a>
          <a href="../contact"><li>CONTACTS</li></a>
        </ul>
      </div>
    </section>


    <!-- Title Bar -->
    <section>
      <div class="top-menu coco">
        <a href="../">
          <img class="left" src="../images/logo.png">
        </a>
        <ul class="right links">
          <a href='../about-us'><li>about us</li></a>
          <a href='../services'><li>services</li></a>
          <a href="../training"><li>training</li></a>
          <a href='../careers'><li>careers</li></a>
          <a href='../contact'><li>contact</li></a>
        </ul>
      </div>
    </section>

    <section>
      <div class="container-fluid training">
        <div class="text">
          <h1>Become an Expert</h1>
          <br><br>
          <h2>We offer a wide range of netwoking courses</h2>
        </div>
        <div class="row">
          <div class="col-sm-3 course">
            <img src="../images/training/1.jpg">
            <h4>MikroTik Certified Internetwork Engineer (MTCINE)</h4>
          </div>
          <div class="col-sm-3 course">
            <img src="../images/training/2.jpg">
            <h4>MikroTik Certified IPv6 Engineer (MTCIPv6E)</h4>
          </div>
          <div class="col-sm-3 course">
            <img src="../images/training/3.png">
            <h4>Mikrotik Certified Network Associate (MTCNA)</h4>
          </div>
          <div class="col-sm-3 course">
            <img src="../images/training/4.png">
            <h4>MikroTik Certififed Routing Engineer (MTCRE)</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 course">
            <img src="../images/training/5.png">
            <h4>MikroTik Certified Traffic Control Engineer (MTCTCE)</h4>
          </div>
          <div class="col-sm-3 course">
            <img src="../images/training/6.png">
            <h4>MikroTik Certified Wireless Engineer (MTCWE)</h4>
          </div>
          <div class="col-sm-3 course">
            <img src="../images/training/7.jpg">
            <h4>Ubiquiti Broadband Wireless Specialist (UBWS)</h4>
          </div>
          <div class="col-sm-3 course">
            <img src="../images/training/8.png">
            <h4>Ubiquiti Broadband Wireless Admin UBWA</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 course">
            <img src="../images/training/9.jpg">
            <h4>Ubiquiti Broadband Routing & Switching Specialist (UBRSS)</h4>
          </div>
          <div class="col-sm-3 course">
            <img src="../images/training/10.jpg">
            <h4>Ubiquiti Broadband Routing & Switching Admin (UBRSA)</h4>
          </div>
          <div class="col-sm-3 course">
            <img src="../images/training/11.png">
            <h4>Ubiquiti Enterprise Wireless Admin UEWA</h4>
          </div>
        </div>
        <div class="call_away">
          <a href="../contact" class="button">CALL US NOW</a>
        </div>
      </div>
    </section>








    <!-- Footer Section -->
    <section>
      <footer>
        <p>&#169; Onspot Solutions 2018 | Designed and developed by <a href='http://www.codydevelopers.co.ke' target="_blank">Cody Developers</a></p>
      </footer>
    </section>


    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>AOS.init();</script>
  </body>
</html>
