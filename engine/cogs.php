<?php

# function to clean data
function clean_data($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  $date = str_replace("'","`", $data);
  return $data;
}

# function to post data
if (isset($_POST['feedback'])) {

  global $conn;

  $message_data = json_decode($_POST['feedback']);

  $name = clean_data($message_data->name);
  $phone = clean_data($message_data->phone);
  $package = clean_data($message_data->package);
  $message = clean_data($message_data->message);


  if ($package == 'other' || $package == '') {
    $subject = 'Website Feedback';
    $value = null;
  }else {
    $subject = 'Potential Client';
    $value = '<p><b>Package Requested:</b>'.$package.' MBPS</p>';
  }

  $to = 'hello@onspotsolutions.co.ke'; // note the comma



// Message
$message = '
<html>
<head>
  <title>'.$subject.'</title>
  <style media="screen">
    body{
      background-color:#cdcdcd;
      text-align:center!important;
    }
    .message_body{
      background-color:#fff;
      padding:5%;
      box-shadow:0 0 5px 2px rgba(0, 0, 0, 0.2);
      font:1.5em "Comfortaa", open sans;
      margin:5% auto!important;
    }

    .message_body img{
      width:200px;
    }
  </style>
</head>
<body>
  <div class="message_body" style="text-align:center;">
    <img src="http://onspotsolutions.co.ke/development/images/logo.png" alt="Onspot Solutions">
    '.$value.'
    <p>'.$message.'</p>

    Regards,
    <p>'.$name.', '.$phone.'</p>
  </div>
</body>
</html>
';

// To send HTML mail, the Content-type header must be set
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';

// Additional headers
$headers[] = 'To: <hello@onspotsolutions.co.ke>';
$headers[] = 'From: Onspot Solutions <noreply@onspotsolutions.co.ke>';

// Mail it
if(@mail($to, $subject, $message, implode("\r\n", $headers))){
  echo 1;
}else{
  echo "Mail Not Sent";
}


}


#

 ?>
