<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Careers</title>
    <meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
    <meta name="description" content="Your Technology Solutions Partner">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css">
    <script type="text/javascript" src="../js/script.js"></script>
    <link rel="shortcut icon" href="../favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>


    <!-- Mobile Menu Section -->
    <section>
      <!-- Hamburger Menu -->
      <div class="hamburger-menu">
        <i class="fa fa-bars"></i>
      </div>

      <div class="hidden-menu">
        <div class="closeit right">
          <i class="fa fa-times" title="close"></i>
        </div>
        <ul>
          <h3>MENU</h3>
          <br><br>
          <a href="../"><li>HOME</li></a>
          <a href="../about-us"><li>ABOUT US</li></a>
          <a href="../services"><li>SERVICES</li></a>
          <a href="../training"><li>TRAINING</li></a>
          <a href="../careers"><li>CAREERS</li></a>
          <a href="../contact"><li>CONTACTS</li></a>
        </ul>
      </div>
    </section>


    <!-- Title Bar -->
    <section>
      <div class="top-menu">
        <a href="../">
          <img class="left white" src="../images/logo2.png">
          <img class="left regular" src="../images/logo.png">
        </a>
        <ul class="right links">
          <a href='../about-us'><li>about us</li></a>
          <a href='../services'><li>services</li></a>
          <a href="../training"><li>training</li></a>
          <a href='../careers'><li>careers</li></a>
          <a href='../contact'><li>contact</li></a>
        </ul>
      </div>
    </section>


    <!-- Welcome to careers -->
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12 welcome_to_careers">
          </div>
        </div>
      </div>
    </section>


    <section>
      <div class="container-fluid">
        <div class="row careers">
          <h2>Become part of the Onspot Team</h2>
          <br><br>
          <div class="col-sm-10 job" data-aos='fade-up' data-aos-duration="1000">
            <h3>Sales and Marketing Personel</h3>
            <h4>Job Description</h4>
            <ul>
              <li>Identify new marketing channels and opportunity in line with the company’s target market.</li>
              <li>Have the ability of closing the deals.</li>
              <li>Focus on relationship management and trust building with existing and potential clients.</li>
              <li>Collect potential customer’s data in the field.</li>
            </ul>
            <h4>Requirements</h4>
            <ul>
              <li>Great sales and marketing skills.</li>
              <li>Excellent communication skills.</li>
              <li>An outgoing and persuasive manner and the ability to deal with people in field.</li>
            </ul>
            <p>If you believe that you can clearly demonstrate your abilities to meet the relevant criteria
              for this position, then please send your CV to <span>recruitment@onspotsolutions.co.ke</span>.</p>
          </div>
        </div>
      </div>
    </section>



    <!-- Footer Section -->
    <section>
      <footer>
        <p>&#169; Onspot Solutions 2018 | Designed and developed by <a href='http://www.codydevelopers.co.ke' target="_blank">Cody Developers</a></p>
      </footer>
    </section>


    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>AOS.init();</script>
  </body>
</html>
