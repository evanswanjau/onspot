<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>About Us</title>
    <meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
    <meta name="description" content="Your Technology Solutions Partner">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css">
    <script type="text/javascript" src="../js/script.js"></script>
    <link rel="shortcut icon" href="../favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>


    <!-- Mobile Menu Section -->
    <section>
      <!-- Hamburger Menu -->
      <div class="hamburger-menu">
        <i class="fa fa-bars"></i>
      </div>

      <div class="hidden-menu">
        <div class="closeit right">
          <i class="fa fa-times" title="close"></i>
        </div>
        <ul>
          <h3>MENU</h3>
          <br><br>
          <a href="../"><li>HOME</li></a>
          <a href="../about-us"><li>ABOUT US</li></a>
          <a href="../services"><li>SERVICES</li></a>
          <a href="../training"><li>TRAINING</li></a>
          <a href="../careers"><li>CAREERS</li></a>
          <a href="../contact"><li>CONTACTS</li></a>
        </ul>
      </div>
    </section>


    <!-- Title Bar -->
    <section>
      <div class="top-menu">
        <a href="../">
          <img class="left white" src="../images/logo2.png">
          <img class="left regular" src="../images/logo.png">
        </a>
        <ul class="right links">
          <a href='../about-us'><li>about us</li></a>
          <a href='../services'><li>services</li></a>
          <a href="../training"><li>training</li></a>
          <a href='../careers'><li>careers</li></a>
          <a href='../contact'><li>contact</li></a>
        </ul>
      </div>
    </section>



    <!-- Welcome to about page -->
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12 welcome_to_about">
            <div class="text">
              <h1 data-aos='fade-up' data-aos-duration="1000">We Build Relationships</h1>
              <p  data-aos='fade-up' data-aos-duration="1500">We partner with you to create connections<br>
              that improve your productivity at home or in your business</p>
              <br>
              <button data-aos='fade-up' data-aos-duration="2000" class="button about">explore</button>
            </div>
          </div>
        </div>
      </div>
    </section>


    <!-- About contact -->
    <section>
      <div class="container-fluid">
        <div class="about_border">
          <div class="row about_us" data-aos='fade-up' data-aos-duration="1000">
            <div class="col-sm-5">
              <img src="../images/mast.jpg" alt="mast">
            </div>
            <div class="col-sm-7 text">
              <h1>We provide the best practices in network security.</h1>
              <p>From our high end equipment, advance technology methods and our team of experts. We always
              put in our effort to ensure that our clients are satisfied with our services. For this to happen, we believe
            that the client always comes first.</p>
            </div>
          </div>
          <br><br>
          <div class="row about_us" data-aos='fade-up' data-aos-duration="1000">
            <div class="col-sm-7 text">
              <h1>Long lasting customer relationships</h1>
              <p>We consider our clients as part of the larger community. Having been connected through us we ensure that our clients
              are always happy by making them feel comfortable. We provide channels where they can comunicate with us for any information that they may need
            or issues that they may have.</p>
            </div>
            <div class="col-sm-5">
              <img src="../images/connection.jpg" alt="mast">
            </div>
          </div>
        </div>
      </div>
    </section>


    <!-- Onspot Logo -->
    <section>
      <div class="container">
        <div class="row">
          <div class="company_logo" data-aos='fade-up' data-aos-duration="1000">
            <img src="../images/logo2.png">
          </div>
        </div>
      </div>
    </section>





    <!-- Footer Section -->
    <section>
      <footer>
        <p>&#169; Onspot Solutions 2018 | Designed and developed by <a href='http://www.codydevelopers.co.ke' target="_blank">Cody Developers</a></p>
      </footer>
    </section>


    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>AOS.init();</script>
  </body>
</html>
