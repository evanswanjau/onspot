<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Services</title>
    <meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
    <meta name="description" content="Your Technology Solutions Partner">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css">
    <script type="text/javascript" src="../js/script.js"></script>
    <link rel="shortcut icon" href="../favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>


    <!-- Mobile Menu Section -->
    <section>
      <!-- Hamburger Menu -->
      <div class="hamburger-menu">
        <i class="fa fa-bars"></i>
      </div>

      <div class="hidden-menu">
        <div class="closeit right">
          <i class="fa fa-times" title="close"></i>
        </div>
        <ul>
          <h3>MENU</h3>
          <br><br>
          <a href="../"><li>HOME</li></a>
          <a href="../about-us"><li>ABOUT US</li></a>
          <a href="../services"><li>SERVICES</li></a>
          <a href="../training"><li>TRAINING</li></a>
          <a href="../careers"><li>CAREERS</li></a>
          <a href="../contact"><li>CONTACTS</li></a>
        </ul>
      </div>
    </section>


    <!-- Title Bar -->
    <section>
      <div class="top-menu">
        <a href="../">
          <img class="left white" src="../images/logo2.png">
          <img class="left regular" src="../images/logo.png">
        </a>
        <ul class="right links">
          <a href='../about-us'><li>about us</li></a>
          <a href='../services'><li>services</li></a>
          <a href="../training"><li>training</li></a>
          <a href='../careers'><li>careers</li></a>
          <a href='../contact'><li>contact</li></a>
        </ul>
      </div>
    </section>


    <!-- Welcome to services -->
    <section>
      <div class="container-fluid">
        <div class="row welcome_to_services">
          <div class="col-sm-12 text">
            <h1 data-aos='fade-up' data-aos-duration="1000"><span>OUR SERVICES<span></h1>
              <br><br>
            <h3 data-aos='fade-up' data-aos-duration="2000"><span>We provide the best internet services that scale your business<span></h3>
          </div>
        </div>
      </div>
    </section>


    <!-- Packages section -->
    <div class="container" id="packages">
      <div class="row packages" data-aos='fade-up' data-aos-duration="2000">
        <h1>AFFORDABLE INTERNET PACKAGES</h1>
        <br><br><br>
        <div class="col-sm-3">
          <img src="../images/bronze.png" alt="bronze">
          <h3>BRONZE PACKAGE</h3>
          <h3>1 MBPS</h3>
        </div>
        <div class="col-sm-3">
          <img src="../images/silver.png" alt="bronze">
          <h3>SILVER PACKAGE</h3>
          <h3>2 MBPS</h3>
        </div>
        <div class="col-sm-3">
          <img src="../images/gold.png" alt="bronze">
          <h3>GOLD PACKAGE</h3>
          <h3>3 MBPS</h3>
        </div>
        <div class="col-sm-3">
          <img src="../images/platinum.png" alt="bronze">
          <h3>PLATINUM PACKAGE</h3>
          <h3>5 MBPS</h3>
        </div>
        <p>&nbsp;</p>
        <br><br>
        <a href="../contact" class="button">ORDER NOW</a>
      </div>
    </div>



    <!-- Other Packages-->
    <section>
      <div class="container-fluid services">
        <div class="row">
          <div class="col-sm-4 service" data-aos='fade-right' data-aos-duration="1000">
            <i class="fa fa-wifi"></i>
            <h3><span>DEDICATED INTERNET SERVICES<span></h3>
            <p>We are specialists in high speed wireless internet connections to businesses of all sizes. We offer resilient,
              cost-effective services at speeds from 1 Mbps up to 10 Mbps.</p>
          </div>
          <div class="col-sm-4 service" data-aos='fade-up' data-aos-duration="1000">
            <i class="fa fa-video"></i>
            <h3><span>CCTV INSTALLATION<span></h3>
              <p>We provide professional state of the art CCTV systems for both residential and commercial
                clientele. We design and install them to your specific requirements.</p>
          </div>
          <div class="col-sm-4 service" data-aos='fade-right' data-aos-duration="1000">
            <i class="fa fa-project-diagram"></i>
            <h3><span>STRUCTURED CABLING<span></h3>
              <p>We offer quality managed cabling. We design, implement and install cabling
                systems using leading high quality products available today.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4 service" data-aos='fade-right' data-aos-duration="1000">
            <i class="fa fa-headset"></i>
            <h3><span>ICT CONSULTANCY<span></h3>
              <p>Our team of highly qualified consultants can help you
                get more value from our IT services by giving advice on your existing network package.</p>
          </div>
          <div class="col-sm-4 service" data-aos='fade-up' data-aos-duration="1000">
            <i class="fa fa-graduation-cap"></i>
            <h3><span>TRAINING<span></h3>
              <p>We also provide accredited training across a wide range of courses and industries
                 to help you improve personal competence and skills in networking.</p>
          </div>
          <div class="col-sm-4 service" data-aos='fade-left' data-aos-duration="1000">
            <i class="fa fa-sitemap"></i>
            <h3><span>WIDE AREA NETWORKS<span></h3>
              <p>With our level of expertise, we have the capability of providing WAN private data network solutions.
                Delivering flexible, secure and centrally managed inter-site connectivity.</p>
          </div>
        </div>
      </div>
    </section>



    <!-- one call away -->
    <section>
      <div class="container-fluid">
        <div class="row call_away">
          <a href="../contact" class="button">WE ARE ONE CALL AWAY</a>
        </div>
      </div>
    </section>




    <!-- Footer Section -->
    <section>
      <footer>
        <p>&#169; Onspot Solutions 2018 | Designed and developed by <a href='http://www.codydevelopers.co.ke' target="_blank">Cody Developers</a></p>
      </footer>
    </section>


    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>AOS.init();</script>
  </body>
</html>
