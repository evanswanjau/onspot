<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Onspot Solutions | Your Technology Solutions Partner</title>
    <meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
    <meta name="description" content="Your Technology Solutions Partner">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <link rel="shortcut icon" href="favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>

    <!-- Mobile Menu Section -->
    <section>
      <!-- Hamburger Menu -->
      <div class="hamburger-menu">
        <i class="fa fa-bars"></i>
      </div>

      <div class="hidden-menu">
        <div class="closeit right">
          <i class="fa fa-times" title="close"></i>
        </div>
        <ul>
          <h3>MENU</h3>
          <br><br>
          <a href=""><li class="current">HOME</li></a>
          <a href="about-us"><li>ABOUT US</li></a>
          <a href="services"><li>SERVICES</li></a>
          <a href="training"><li>TRAINING</li></a>
          <a href="careers"><li>CAREERS</li></a>
          <a href="contact"><li>CONTACTS</li></a>
        </ul>
      </div>
    </section>


    <!-- Title Bar -->
    <section>
      <div class="top-menu">
        <a href="">
          <img class="left white" src="images/logo2.png">
          <img class="left regular" src="images/logo.png">
        </a>
        <ul class="right links">
          <a href='about-us'><li>about us</li></a>
          <a href='services'><li>services</li></a>
          <a href="training"><li>training</li></a>
          <a href='careers'><li>careers</li></a>
          <a href='contact'><li>contact</li></a>
        </ul>
      </div>
    </section>



    <!-- Slider container | Welcome container -->
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12 image_container">
            <div class="text">
              <h1 data-aos='fade-up' data-aos-duration="1000"><span>Technology solutions for both businesses and homes</span></h1>
            </div>
            <div class="scroll" data-aos='fade-up' data-aos-duration="3000">
              <i class="fa fa-angle-down"></i>
            </div>
          </div>
        </div>
      </div>
    </section>



    <!-- What we can do for you -->
    <section>
      <div class="container-fluid">
        <div class="row what_we_can_do">
          <h2><span>WHAT</span> WE PROVIDE</h2>
          <br><br>

          <div class="col-sm-4">
            <div class="point"data-aos='zoom-in-right' data-aos-duration="2000">
              <div class="logo">
                <i class="fa fa-wifi"></i>
              </div>

              <h3>INTERNET CONNECTIVITY</h3>
              <p>We provide fast internet connections, our latest high end gear
                provides reliable and accurate performance</p>
                <br>
                <a href="services" class="button">view more</a>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="point" data-aos='zoom-in-up' data-aos-duration="2000">
              <div class="logo">
                <i class="fa fa-video"></i>
              </div>

              <h3>CCTV INSTALLATION</h3>
              <p>We deal with surveillance equipment, installations, maintenance and security.</p>
                 <br>
                 <a href="services" class="button">view more</a>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="point" data-aos='zoom-in-left' data-aos-duration="2000">
              <div class="logo">
                <i class="fa fa-desktop"></i>
              </div>

              <h3>ICT CONSULTANCY</h3>
              <p>We focus on giving technical advice to organizations in achieving their business objectives</p>
                <br>
                <a href="services" class="button">view more</a>
            </div>
          </div>
        </div>
      </div>
    </section>



    <!-- Quick question -->
    <section>
      <div class="container-fluid" data-aos='fade-up' data-aos-duration="2000">
        <div class="row quick_question">
          <a href="services#packages" class="button">CHECK OUT OUR PACKAGES</a>&nbsp;&nbsp;<a href="contact" class="button">QUESTION? CONTACT US</a>
        </div>
      </div>
    </section>



    <!-- Why you want to work with us -->
    <section>
      <div class="container-fluid">
        <div class="row why_us">
          <div class="col-sm-6 why_us_image" data-aos='fade-down' data-aos-duration="2000">
            <h1><span>WHY US?</span> BECAUSE WE CARE</h1>
          </div>
          <div class="col-sm-6 why_us_text" data-aos='fade-up' data-aos-duration="2000">
            <p>More than just service provision, we take our time to get to know our customers
            and give the best of what we can offer. Our focus is to bridge as many gaps as possible,
          making the world a smaller space where people can communicate and transact with each other.</p>

              <a href="about-us">READ MORE ABOUT US</a>
          </div>
        </div>
      </div>
    </section>



    <!-- Our Clients -->
    <section>
        <div class="container-fluid">
          <div class="row our_clients" data-aos='fade-down' data-aos-duration="2000">
            <h2>OUR PARTNERS</h2>
            <br><br><br><br>
            <div class="col-sm-2 client">
              <img src="images/1.png" alt="Rock Beach Resort">
            </div>
            <div class="col-sm-2 client">
              <img src="images/2.png" alt="Standard Consulting Group Limited">
            </div>
            <div class="col-sm-2 client">
              <img src="images/3.png" alt="The Yard Bar and Grill">
            </div>
            <div class="col-sm-2 client">
              <img src="images/4.png" alt="Circular Connections">
            </div>
            <div class="col-sm-2 client">
              <img src="images/5.png" alt="Galaxy Vip Lounge">
            </div>
            <div class="col-sm-2 client">
              <img src="images/6.png" alt="Tracc Palace">
            </div>
          </div>
        </div>
    </section>


    <!-- Testimonials -->
    <section>
      <div class="container-fluid">
        <div class="row testimonials"  data-aos='fade-up' data-aos-duration="2000">
          <div class="col-sm-12 testimony 3">
            <img src="images/profile3.jpg" alt="Evans Wanjau">
            <h4><span>James Kitenge | CEO Bills Inc</span></h4>
            <br>
            <p>"You made the process so simple. My new new internet connection is so much faster and easier to work with
              than my old network. Thanks, guys! "</p>
          </div>
          <div class="col-sm-12 testimony 2">
            <img src="images/profile2.jpg" alt="Evans Wanjau">
            <h4><span>Peter Karani | Business Owner</span></h4>
            <br>
            <p>"With the new upgrades Onspot solutions made, I can confidently say that my internet cafe has seen tremendous
              improvements. Feedback from my customers is good."</p>
          </div>
          <div class="col-sm-12 testimony 1">
            <img src="images/profile1.jpg" alt="Evans Wanjau">
            <h4><span>Mary Atieno | Entreprenuer</span></h4>
            <br>
            <p>"Onspot solutions have made our life easier. We can now make trades in realtime
              and make returns as soon as they are requested. Thank you Onspot"</p>
          </div>
        </div>
      </div>
    </section>


    <section>
      <div class="col-sm-12 my_form" data-aos='fade-up' data-aos-duration="2000" style="overflow:hidden!important;">
        <h3>Choose a package or just say Hi!</h3>
        <br>
        <form class="ui-form" action="" name="myForm" method="post">
          <p id="message"></p>
          <div id="sendMessage">
            <input type="text" name="name" placeholder="name" id="inputName" onkeyup="inputValue(this.value, 'inputName')">
            <input type="text" name="phone" placeholder="phone" id="inputPhone" onkeyup="inputValue(this.value, 'inputPhone')">
            <br><br>
            <select class="dropper" name="package" id="inputPackage" onchange="inputValue(this.value, 'inputPackage')">
              <option value="">choose package</option>
              <option value="1">1 MPS</option>
              <option value="2">2 MPS</option>
              <option value="3">3 MPS</option>
              <option value="4">4 MPS</option>
              <option value="5">5 MPS</option>
              <option value="10">10 MPS</option>
              <option value="other">Other</option>
            </select>
            <br><br>
            <textarea name="message" rows="4" cols="80" placeholder="Send us a message - Max 200 characters" id="inputMessage" onkeyup="inputValue(this.value, 'inputMessage')"></textarea>
            <br><br>
            <input class="button" type="submit" name="submit" value="submit" onclick="return sendMessage()">
          </div>
        </form>
      </div>
    </section>

    <!-- Footer Section -->
    <section>
      <footer>
        <p>&#169; Onspot Solutions 2018 | Designed and developed by <a href='http://www.codydevelopers.co.ke' target="_blank">Cody Developers</a></p>
      </footer>
    </section>


    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>AOS.init();</script>
  </body>
</html>
