<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Contact</title>
    <meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
    <meta name="description" content="Your Technology Solutions Partner">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/main.css">
    <script type="text/javascript" src="../js/script.js"></script>
    <script type="text/javascript" src="../js/main.js"></script>
    <link rel="shortcut icon" href="../favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>


    <!-- Mobile Menu Section -->
    <section>
      <!-- Hamburger Menu -->
      <div class="hamburger-menu">
        <i class="fa fa-bars"></i>
      </div>

      <div class="hidden-menu">
        <div class="closeit right">
          <i class="fa fa-times" title="close"></i>
        </div>
        <ul>
          <h3>MENU</h3>
          <br><br>
          <a href="../"><li>HOME</li></a>
          <a href="../about-us"><li>ABOUT US</li></a>
          <a href="../services"><li>SERVICES</li></a>
          <a href="../training"><li>TRAINING</li></a>
          <a href="../careers"><li>CAREERS</li></a>
          <a href="../contact"><li>CONTACTS</li></a>
        </ul>
      </div>
    </section>


    <!-- Map Location -->
    <section>
      <div class="container-fluid">
        <div class="row google_map">
          <div class="map_cover">



            <!-- Title Bar -->
            <section>
              <div class="top-menu coco">
                <a href="../">
                  <img class="left" src="../images/logo.png">
                </a>
                <ul class="right links">
                  <a href='../about-us'><li>about us</li></a>
                  <a href='../services'><li>services</li></a>
                  <a href="../training"><li>training</li></a>
                  <a href='../careers'><li>careers</li></a>
                  <a href='../contact'><li>contact</li></a>
                </ul>
              </div>
            </section>


            <div class="row classified_holder">
              <!-- contact info -->
              <section>
                <div class="col-sm-4 contact_menu" data-aos='fade-up' data-aos-duration="2000">
                  <h3>We are always available to give you the support that you need</h3>
                  <br><br>
                    <ul>
                      <li><i class="fa fa-phone left"></i> 0705 510 070 / 0752 232 556</li>
                      <li><i class="fa fa-envelope left"></i> support@onspotsolutions.co.ke</li>
                      <li><i class="fa fa-map-marker-alt left"></i> 1st floor - Unaitas Building, Juja</li>
                    </ul>
                </div>
              </section>


              <!-- contact form -->
              <section>
                <div class="col-sm-6 contact_form" data-aos='fade-left' data-aos-duration="2000">
                  <h3>Choose a package or just say Hi!</h3>

                  <form class="ui-form" action="" name="myForm" method="post">
                    <p id="message"></p>
                    <div id="sendMessage">
                      <input type="text" name="name" placeholder="name" id="inputName" onkeyup="inputValue(this.value, 'inputName')">
                      <input type="text" name="phone" placeholder="phone" id="inputPhone" onkeyup="inputValue(this.value, 'inputPhone')">
                      <br><br>
                      <select class="dropper" name="package" id="inputPackage" onchange="inputValue(this.value, 'inputPackage')">
                        <option value="">choose package</option>
                        <option value="1">1 MPS</option>
                        <option value="2">2 MPS</option>
                        <option value="3">3 MPS</option>
                        <option value="4">4 MPS</option>
                        <option value="5">5 MPS</option>
                        <option value="10">10 MPS</option>
                        <option value="other">Other</option>
                      </select>
                      <br><br>
                      <textarea name="message" rows="4" cols="80" placeholder="Send us a message - Max 200 characters" id="inputMessage" onkeyup="inputValue(this.value, 'inputMessage')"></textarea>
                      <br><br>
                      <input class="button" type="submit" name="submit" value="submit" onclick="return sendMessage()">
                    </div>
                  </form>
                </div>
              </section>
            </div>



            <!-- Footer Section -->
            <br><br>
            <section>
              <footer>
                <p>&#169; Onspot Solutions 2018 | Designed and developed by <a href='http://www.codydevelopers.co.ke' target="_blank">Cody Developers</a></p>
              </footer>
            </section>
          </div>



          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1237.943018037915!2d37.01325725624005!3d-1.102114899922574!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f47ad7d5b368d%3A0x991c3b04db20cecf!2sOnspot+Solutions+Offices!5e0!3m2!1sen!2ske!4v1531575534687" frameborder="0" style="border:0"></iframe>
        </div>
      </div>
    </section>



    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>AOS.init();</script>
  </body>
</html>
